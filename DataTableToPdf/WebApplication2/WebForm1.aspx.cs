﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private static string connectionString = String.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public static string ConnectionString
        {
            get
            {
                if (connectionString.Length == 0)
                {
                    connectionString = ConfigurationManager.ConnectionStrings["MultiRev"].ToString();  
                }
                return connectionString;
            }
            set
            {
                connectionString = value;
            }
        }
        public static DataSet ExecuteDataset(string strCommandText)
        {
            return MySqlHelper.ExecuteDataset(ConnectionString, strCommandText);
        }
        public DataSet getSuccessfulDownload()
        {
            DataSet ds = new DataSet();
            //string ndate = DateTime.Now.ToString("yyyy-MM-dd");
            string ndate = "2017-01-06";
            string query = "Select Rev_TID,Remarks,Date,time,AppVersion,RemoteIp from download_History where Date = '" + ndate + "' and Status ='" + 0 + "'order by Date DESC";
            //string query = "Select Rev_TID,Remarks,Date,time,AppVersion,Remarks,RemoteIp from download_History where Status=0 order by Date DESC";
            try
            {
                ds = ExecuteDataset(query);
                ds.Tables[0].TableName = "Successful Download Report";
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ds.Dispose();
            }
        }

        public DataSet getFailedDownload()
        {
            DataSet ds = new DataSet();
            string ndate = DateTime.Now.ToString("yyyy-MM-dd");
            string query = "Select Rev_TID,Remarks,Date,time,AppVersion,RemoteIp from download_History where Date = '" + ndate + "' and Status ='" + 1 + "'order by Date DESC";
            //string query = "Select Rev_TID,Remarks,Date,time,AppVersion,Remarks,RemoteIp from download_History where Status=1 order by Date DESC";
            try
            {
                ds = ExecuteDataset(query);
                ds.Tables[0].TableName = "Failed Download Report";
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ds.Dispose();
            }
        }

        public DataSet getDownloadMerchantWise()
        {
            DataSet ds = new DataSet();
            string ndate = DateTime.Now.ToString("yyyy-MM-dd");
            string query = "Select POS_Serial as Merchant,Count(*) as DownloadCount from download_History where Date = '" + ndate + "' and Status ='" + 0 + "'group by POS_Serial DESC";
            //string query = "Select POS_Serial as Merchant,Count(*) as DownloadCount from download_History where Status=0 group by POS_Serial DESC";
            try
            {
                ds = ExecuteDataset(query);
                ds.Tables[0].TableName = "Merchant Download Report";
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ds.Dispose();
            }
        }

        public DataSet getDownloadTIDWise()
        {
            DataSet ds = new DataSet();
            string ndate = DateTime.Now.ToString("yyyy-MM-dd");
            string query = "Select Rev_TID as TerminalId,Count(*) as DownloadCount from download_History where Date = '" + ndate + "' and Status ='" + 0 + "'group by Rev_TID DESC";
           //string query = "Select Rev_TID as TerminalId,Count(*) as DownloadCount from download_History where Status=0 group by Rev_TID DESC";
            try
            {
                ds = ExecuteDataset(query);
                ds.Tables[0].TableName = "TerminalId Download Report";
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ds.Dispose();
            }
        }

        public DataTable GetTable()
        {
            DataSet dt = getSuccessfulDownload();
            DataTable dt1 = dt.Tables[0];
            return dt1;
        }

        public void ExportToPdf(DataTable dataTable)
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);
            try
            {
                string name = dataTable.TableName;
                PdfWriter.GetInstance(pdfDoc, System.Web.HttpContext.Current.Response.OutputStream);
                pdfDoc.Open();
                Chunk c = new Chunk(name + System.Web.HttpContext.Current.Session["CompanyName"] + "", FontFactory.GetFont(FontFactory.TIMES, 11));
                Paragraph p = new Paragraph();
                p.Alignment = Element.ALIGN_CENTER;
                p.Add(c);
                pdfDoc.Add(p);
                Chunk c1 = new Chunk("Reported Generated on " + DateTime.Now.ToString(), FontFactory.GetFont(FontFactory.TIMES, 11));
                Paragraph p1 = new Paragraph();
                p1.Alignment = Element.ALIGN_RIGHT;
                p1.Add(c1);
                pdfDoc.Add(p1);
                //--- Add Logo of PDF ----
                string imageFilePath = System.Web.HttpContext.Current.Server.MapPath("//Content/image003.jpg");
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);
                //Resize image depend upon your need
                jpg.ScaleToFit(80f, 60f);
                //Give space before image
                jpg.SpacingBefore = 0f;
                //Give some space after the image
                jpg.SpacingAfter = 1f;
                jpg.Alignment = Element.HEADER;
                pdfDoc.Add(jpg);
                iTextSharp.text.Font font8 = FontFactory.GetFont("ARIAL", 7);
                //--- Add new Line ------------
                Phrase phrase1 = new Phrase(Environment.NewLine);
                pdfDoc.Add(phrase1);
                DataTable dt = dataTable;
                if (dt != null)
                {
                    //---- Add Result of DataTable to PDF file With Header -----
                    PdfPTable pdfTable = new PdfPTable(dt.Columns.Count);
                    pdfTable.DefaultCell.Padding = 5;
                    pdfTable.WidthPercentage = 100; // percentage
                    pdfTable.DefaultCell.BorderWidth = 2;
                    pdfTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

                    foreach (DataColumn column in dt.Columns)
                    {
                        pdfTable.AddCell(FormatHeaderPhrase(column.ColumnName));
                    }
                    pdfTable.HeaderRows = 1; // this is the end of the table header
                    pdfTable.DefaultCell.BorderWidth = 1;

                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (object cell in row.ItemArray)
                        {
                            //assume toString produces valid output
                            pdfTable.AddCell(FormatPhrase(cell.ToString()));
                        }
                    }
                    pdfDoc.Add(pdfTable);
                }
                pdfDoc.Close();
                System.Web.HttpContext.Current.Response.ContentType = "application/pdf";
                System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename= Download Report.pdf");
                System.Web.HttpContext.Current.Response.Write(pdfDoc);
                System.Web.HttpContext.Current.Response.Flush();
                System.Web.HttpContext.Current.Response.End();
                //HttpContext.Current.ApplicationInstance.CompleteRequest();  
            }
            catch (DocumentException de)
            {
                System.Web.HttpContext.Current.Response.Write(de.Message);
            }
            catch (IOException ioEx)
            {
                System.Web.HttpContext.Current.Response.Write(ioEx.Message);
            }
            catch (Exception ex)
            {
                System.Web.HttpContext.Current.Response.Write(ex.Message);
            }
        }

        public static void SaveDataTableToPDF(DataTable DTable)
        {
            // string filepath =System.Web.HttpContext.Current.Server.MapPath("//Content/");
            string filepath = @"C:\\PDF\\{Report.pdf}";
            FontFactory.RegisterDirectories();
            iTextSharp.text.Font myfont = FontFactory.GetFont("Tahoma", BaseFont.IDENTITY_H, 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);

            pdfDoc.Open();
            PdfWriter wri = PdfWriter.GetInstance(pdfDoc, new FileStream(filepath, FileMode.Create));
            pdfDoc.Open();
            PdfPTable _mytable = new PdfPTable(DTable.Columns.Count);

            for (int j = 0; j < DTable.Columns.Count; ++j)
            {
                Phrase p = new Phrase(DTable.Columns[j].ColumnName, myfont);
                PdfPCell cell = new PdfPCell(p);
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                _mytable.AddCell(cell);
            }
            //-------------------------
            for (int i = 0; i < DTable.Rows.Count - 1; ++i)
            {
                for (int j = 0; j < DTable.Columns.Count; ++j)
                {

                    Phrase p = new Phrase(DTable.Rows[i][j].ToString(), myfont);
                    PdfPCell cell = new PdfPCell(p);
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    _mytable.AddCell(cell);
                }
            }
            //------------------------           
            pdfDoc.Add(_mytable);
            pdfDoc.Close();
            System.Diagnostics.Process.Start(filepath);
        }
        private Phrase FormatPhrase(string value)
        {
            return new Phrase(value, FontFactory.GetFont(FontFactory.TIMES,11));
        }

        private static Phrase FormatHeaderPhrase(string value)
        {
            return new Phrase(value, FontFactory.GetFont(FontFactory.TIMES, 11, iTextSharp.text.Font.UNDERLINE, new iTextSharp.text.BaseColor(0, 0, 255)));
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            DataTable dt = GetTable();
            SaveDataTableToPDF(dt);
        }
    }
}